const mapObject = (obj, cb) => {
    const newObject = {};
    for (key in obj) {
        newObject[key] = cb(key, obj);
    }
    return newObject;
}

module.exports = mapObject;